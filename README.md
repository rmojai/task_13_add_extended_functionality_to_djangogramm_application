# Task_13_Add_extended_functionality_to_DjangoGramm_application



Modify the application to add new functionality. 
1)User can subscribe to other users. 
2)The user has a news feed where he watches recent events. 
3)Friends(the users I'm subscribed to) post images and subscribe or unsubscribe to other users. 
4)Users can put likes/unliked pictures of other users.
The plan:
Use bootstrap for make a pretty view, etc...
Add following and follower entity 
Add likes entity
Use third party service for storing images 
Deploy the code to the server.

Resources:
Cloudinary https://cloudinary.com/documentation/django_image_and_video_upload

Write tests using Unittest module or py.test.

