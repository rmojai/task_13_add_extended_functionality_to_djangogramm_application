# Generated by Django 4.2 on 2023-05-10 10:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('djapp', '0007_activity_post_activity_profile_alter_activity_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='activity',
            old_name='profile',
            new_name='follow',
        ),
        migrations.AlterField(
            model_name='activity',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='activity', to=settings.AUTH_USER_MODEL),
        ),
    ]
