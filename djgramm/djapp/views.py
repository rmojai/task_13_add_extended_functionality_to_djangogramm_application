from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator as \
    token_generator
from django.contrib.auth.views import LoginView
from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.http import urlsafe_base64_decode
import cloudinary
from cloudinary import uploader

from .forms import *
from .models import *
from .utils import send_email_for_verify, create_activity


def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Page Not Found</h1>')


def home(request):
    user = request.user
    post = Post.objects.all().order_by('-datetime')
    images = Image.objects.all()
    return render(request, 'djapp/home.html', {'user': user, 'post': post, 'images': images})


def add_post(request):
    user = request.user
    tags = Tag.objects.all()

    if request.method == 'POST':
        tag_new = request.POST.get('tag_new')
        photo = request.FILES.getlist('photo')
        description = request.POST.get('description')
        tags_posts = request.POST.getlist('tags_post')

        post = Post(user=user, description=description)
        post.save()
        post_obj = Post.objects.last()

        for i in tags_posts:
            tag_id = Tag.objects.filter(tag=i).first()
            post_obj.tags.add(tag_id)

        for i in photo:
            uploaded_image = cloudinary.uploader.upload(i, folder='djgramm')
            image_url = uploaded_image['secure_url']
            image = Image(post_id=post_obj, photo=image_url)
            image.save()

        if tag_new:
            tag_new_add, _ = Tag.objects.get_or_create(tag=tag_new)
            if tag_new_add:
                post_obj.tags.add(tag_new_add.id)

        create_activity(user=user, verb='added a new post', post=post_obj)
        return redirect('profile', user.username)

    return render(request, 'djapp/add_post.html', {'tags': tags})


def my_post(request, post_id):
    post = Post.objects.filter(id=post_id).first()
    images = Image.objects.filter(post_id=post).all()
    if post:
        if request.method == 'GET':
            return render(request, 'djapp/my_post.html', {'post': post, 'images': images, })
    return pageNotFound


@login_required(login_url='login')
def like_post(request):
    user = request.user
    if request.method == 'POST':
        post_id = request.POST.get('post_id')
        post_obj = Post.objects.get(id=post_id)

        if user in post_obj.liked.filter(id=user.id):
            post_obj.liked.remove(user)
            create_activity(user=user, verb='disliked a post', post=post_obj)
        else:
            post_obj.liked.add(user)
            create_activity(user=user, verb='liked a post', post=post_obj)

    return redirect('home')


def tag_post(request, tag_id):
    tags = Post.objects.filter(tags=tag_id).all()
    tag_name = Tag.objects.filter(id=tag_id).first()
    tag_new = request.POST.get('tag_new')

    if tag_new:
        Tag.objects.get_or_create(tag=tag_new)
        return render(request, 'djapp/tag.html', {'tags': tags, 'tag_name': tag_name})
    if request.method == 'GET':
        return render(request, 'djapp/tag.html', {'tags': tags, 'tag_name': tag_name})
    return pageNotFound


@login_required(login_url='login')
def profile(request, username):
    user = User.objects.filter(username=username).first()
    post = Post.objects.filter(user=user).all().order_by('-datetime')
    if user:
        if request.method == 'GET':
            return render(request, 'djapp/profile.html', {'user': user, 'post': post})
    return pageNotFound


def profile_edit(request, username):
    user = User.objects.filter(username=username).first()

    if user:
        try:
            profile = request.user.profile
        except Profile.DoesNotExist:
            profile = Profile.objects.create(user=request.user)

        if request.method == 'POST':
            form = ProfileEditForm(request.POST, request.FILES, instance=profile)
            if form.is_valid():
                user.first_name = form.cleaned_data['user__first_name']
                user.last_name = form.cleaned_data['user__last_name']
                user.save()

                photo = request.FILES.get('avatar')
                if photo:
                    response = uploader.upload(photo, folder='avatars')
                    profile.avatar = response['url']

                bio = request.POST.get('bio')
                if bio:
                    profile.bio = bio

                profile.save()
                form.save()
                return redirect(reverse('profile', args=[username]))
        form = ProfileEditForm(instance=profile)

        return render(request, 'djapp/profile_edit.html', {'user': user, 'form': form})
    return pageNotFound


class MyLoginView(LoginView):
    form_class = AuthenticationForm


def register(request):
    form = UserCreationForm(request.POST)

    if form.is_valid():
        form.save()
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password1')
        user = authenticate(email=email, password=password)
        Profile.objects.create(user=user)
        send_email_for_verify(request, user)
        return redirect('confirm_email')
    form = form
    return render(request, 'registration/register.html', {'form': form})


def emailverify(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError,
            User.DoesNotExist, ValidationError):
        user = None

    if user is not None and token_generator.check_token(user, token):
        user.email_verify = True
        user.save()
        login(request, user)
        return redirect('profile_edit', user.username)
    return redirect('invalid_verify')


@login_required(login_url='login')
def follow(request, username):
    user = request.user
    if request.method == 'POST':
        profile_id = request.POST.get('follow')
        profile = Profile.objects.get(id=profile_id)

        if user in profile.following.all():
            profile.following.remove(user)
            create_activity(user=user, verb='unfollowing profile', follow=profile)
        else:
            profile.following.add(user)
            create_activity(user=user, verb='following profile', follow=profile)
    return redirect('profile', username=username)


@login_required(login_url='login')
def followers(request, username):
    user = get_object_or_404(User, username=username)
    followers = user.followers.all()
    following = user.profile.following.all()
    context = {
        'user': user,
        'followers': followers,
        'following': following,
    }
    return render(request, 'djapp/followers.html', context)


def feed(request):
    activities = Activity.objects.all().order_by('-datetime')
    return render(request, 'djapp/feed.html', {'activities': activities})
