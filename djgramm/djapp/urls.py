from django.urls import path, include, re_path
from django.views.generic import TemplateView

from .views import *

urlpatterns = [
    path('login/', MyLoginView.as_view(), name='login'),
    path('', home, name='home'),
    path('', include('django.contrib.auth.urls')),
    path('register/', register, name='register'),
    path(
        'invalid_verify/',
        TemplateView.as_view(template_name='registration/invalid_verify.html'),
        name='invalid_verify'
    ),
    path(
        'confirm_email/',
        TemplateView.as_view(template_name='registration/confirm_email.html'),
        name='confirm_email'
    ),
    path('verify_email/<uidb64>/<token>/', emailverify, name='verify_email'),
    path('profile/<username>', profile, name='profile'),
    path('profile_edit/<username>', profile_edit, name='profile_edit'),
    path('add_post/', add_post, name='add_post'),
    path('my_post/<post_id>', my_post, name='my_post'),
    path('like/', like_post, name='like_post'),
    path('tag/<tag_id>', tag_post, name='tag_post'),
    path('follow_profile/<username>', follow, name='follow_profile'),
    path('followers/<username>', followers, name='followers'),
    path('feed/', feed, name='feed'),

]
