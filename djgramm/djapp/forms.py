from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.forms import (
    UserCreationForm as DjangoUserCreationForm,
    AuthenticationForm as DjangoAuthenticationForm,
)
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Profile
from .utils import send_email_for_verify

User = get_user_model()


class UserCreationForm(DjangoUserCreationForm):
    email = forms.EmailField(
        label=_("Email"),
        max_length=254,
        widget=forms.EmailInput(attrs={'autocomplete': 'email'}),
    )

    class Meta(DjangoUserCreationForm.Meta):
        model = User
        fields = ("username", "email")


class AuthenticationForm(DjangoAuthenticationForm):

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(
                self.request,
                username=username,
                password=password,
            )
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

            if not self.user_cache.email_verify:
                send_email_for_verify(self.request, self.user_cache)
                raise ValidationError(
                    'Email not verify, check your email',
                    code='invalid_login',
                )

        return self.cleaned_data


from django.forms.widgets import FileInput


class ProfileEditForm(forms.ModelForm):
    user__first_name = forms.CharField(label='first name')
    user__last_name = forms.CharField(label='last name')

    class Meta:
        model = Profile
        fields = ['avatar', 'bio', 'user__first_name', 'user__last_name']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance:
            self.fields['user__first_name'].initial = instance.user.first_name
            self.fields['user__last_name'].initial = instance.user.last_name
        self.fields['avatar'].required = False
        self.fields['avatar'].widget = FileInput()

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']
        if not avatar:
            return self.instance.avatar
        return avatar

    def save(self, **kwargs):
        profile = super().save(commit=False)
        user = profile.user
        user.first_name = self.cleaned_data['user__first_name']
        user.last_name = self.cleaned_data['user__last_name']
        profile.save()
        user.save()
        return profile
