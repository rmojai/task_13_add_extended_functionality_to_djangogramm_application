from django.contrib.auth.models import AbstractUser
from django.db import models
from cloudinary.models import CloudinaryField
from django.utils.translation import gettext_lazy as _

from djgramm.settings import NONAME_AVATAR


class User(AbstractUser):
    email = models.EmailField(
        _('email address'),
        unique=True,
    )

    email_verify = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']


class Profile(models.Model):
    objects = models.Manager()
    user = models.OneToOneField(User, on_delete=models.PROTECT, related_name='profile')
    bio = models.CharField(max_length=200, null=True, blank=True)
    # avatar = models.ImageField(upload_to='avatar/', blank=True, default=NONAME_AVATAR)
    avatar = CloudinaryField('avatar', folder='avatar', blank=True, default=NONAME_AVATAR)
    following = models.ManyToManyField(User, blank=True, related_name='followers')

    class DoesNotExist(models.ObjectDoesNotExist):
        pass


class Tag(models.Model):
    objects = models.Manager()
    tag = models.CharField(max_length=200)

    def __str__(self):
        return str(self.tag)


class Post(models.Model):
    objects = models.Manager()
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='post')
    datetime = models.DateTimeField(auto_now_add=True)
    description = models.TextField(null=True, blank=True)
    liked = models.ManyToManyField(User, default=None, related_name='liked')
    tags = models.ManyToManyField(Tag, default=None, related_name='tags')

    @property
    def num_likes(self):
        return self.liked.all().count()


class Image(models.Model):
    objects = models.Manager()
    # photo = models.ImageField(upload_to='photo/', null=True, blank=True)
    photo = CloudinaryField('photo', folder='photo', null=True, blank=True)
    post_id = models.ForeignKey(Post, on_delete=models.CASCADE)


class Activity(models.Model):
    objects = models.Manager()
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='activity')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True)
    follow = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True, blank=True)
    verb = models.CharField(max_length=255)
    datetime = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-datetime']
