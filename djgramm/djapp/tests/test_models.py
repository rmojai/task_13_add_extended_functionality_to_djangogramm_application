import pytest

from djapp.models import *


@pytest.mark.django_db
def test_create_user(user):
    assert user.email == 'testuser@example.com'
    assert user.username == 'testuser'
    assert user.check_password('testpass') == True
    assert User.objects.filter(email='testuser@example.com').exists()


@pytest.mark.django_db
def test_username_field(user):
    assert user.get_username() == 'testuser@example.com'


@pytest.mark.django_db
def test_create_profile(user, profile):
    assert profile.user == user
    assert profile.bio == 'Test bio'
    assert profile.avatar == 'test_avatar.jpg'
    assert Profile.objects.filter(user=user).exists()


@pytest.mark.django_db
def test_create_tag(tag):
    assert tag.tag == 'testtag'
    assert Tag.objects.filter(tag='testtag').exists()


@pytest.mark.django_db
def test_create_post(user, post):
    assert post.user == user
    assert post.description == 'descriptionpost'
    assert Post.objects.filter(user=user).exists()


@pytest.mark.django_db
def test_post_likes(userlike, user):
    post_like = Post.objects.create(user=userlike, description='descriptionpost')
    post_like.liked.add(user)
    assert post_like.liked.count() == 1
    assert post_like.num_likes == 1
    assert post_like.liked.filter(email=user.email).exists()


@pytest.mark.django_db
def test_create_tag(tag):
    assert str(tag) == 'testtag'
    assert Tag.objects.filter(tag='testtag').exists()


@pytest.mark.django_db
def test_create_profile(user, profile):
    assert profile.user == user
    assert profile.bio == 'Test bio'
    assert Profile.objects.filter(user=user).exists()


@pytest.mark.django_db
def test_add_tags_to_post(user, post):
    tag1 = Tag.objects.create(tag='Tag1')
    tag2 = Tag.objects.create(tag='Tag2')
    post.tags.add(tag1, tag2)
    assert post.tags.count() == 2
    assert post.tags.filter(tag='Tag1').exists()
    assert post.tags.filter(tag='Tag2').exists()


if __name__ == '__main__':
    pytest.main()
