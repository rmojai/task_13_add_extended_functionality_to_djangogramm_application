import pytest
from django.test import Client
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


@pytest.fixture
def client():
    client = Client()
    return client


@pytest.mark.django_db
def test_home_view(client):
    url = reverse('home')
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_add_post_view(client, user, tag):
    url = reverse('add_post')
    client.force_login(user)
    data = {
        'tag_new': tag.tag,
        'photo': '',
        'description': 'test post',
        'tags_post': [tag.tag]
    }
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_my_post_view(client, user, post):
    url = reverse('my_post', args=[post.id])
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_like_post_view(client, user, post):
    url = reverse('like_post')
    client.force_login(user)
    data = {'post_id': post.id}
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_tag_post_view(client, tag, post):
    url = reverse('tag_post', args=[tag.id])
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_profile_view(client, user):
    url = reverse('profile', args=[user.username])
    client.force_login(user)
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_profile_edit_view(client, user):
    url = reverse('profile_edit', args=[user.username])
    client.force_login(user)
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_register_view(client, user_creation_form):
    url = reverse('register')
    data = user_creation_form
    response = client.post(url, data=data)
    assert response.status_code == 200


@pytest.mark.django_db
def test_emailverify_view(client, user, token_generator):
    token = token_generator.make_token(user)
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    url = reverse('verify_email', args=[uid, token])
    response = client.get(url)
    assert response.status_code == 302


if __name__ == '__main__':
    pytest.main()
