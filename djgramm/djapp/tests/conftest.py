from datetime import datetime

import pytest
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator

from djapp.models import *


@pytest.fixture
def user():
    User = get_user_model()
    user = User.objects.create_user(
        username='testuser',
        email='testuser@example.com',
        password='testpass'
    )
    return user


@pytest.fixture
def userlike(user):
    User = get_user_model()
    user_like = User.objects.create_user(
        username='testuserlike',
        email='testuserlike@example.com',
        password='testpasslike'
    )
    return user_like


@pytest.fixture
def profile(user):
    profile = Profile.objects.create(user=user, bio='Test bio', avatar='test_avatar.jpg')
    return profile


@pytest.fixture
def tag():
    tag = Tag.objects.create(tag='testtag')
    return tag


@pytest.fixture
def post(user):
    post = Post.objects.create(user=user, datetime=datetime.now(), description='descriptionpost')
    return post


@pytest.fixture
def user_creation_form():
    return {
        'username': 'testuser',
        'email': 'testuser@example.com',
        'password1': 'testpass',
        'password2': 'testpass'
    }


@pytest.fixture
def token_generator():
    return PasswordResetTokenGenerator()
